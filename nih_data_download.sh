#!/bin/bash

# Download archives from 1985 - 2013
wget http://exporter.nih.gov/CSVs/final/RePORTER_PRJ_C_FY{1985..2013}.zip

# Download archives from 2014 series 119-138
wget http://exporter.nih.gov/CSVs/final/RePORTER_PRJ_C_FY2014_{119..138}.zip
